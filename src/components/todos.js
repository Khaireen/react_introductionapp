import React, { Component } from 'react';
import TodoItem from './todo-item';

class Todos extends Component {
    render() {
        let todoItems;
        if (this.props.todos) {
            todoItems = this.props.todos.map(todo => {
                return (
                    <TodoItem key={todo.title} todo={todo} />
                );
            });
        }

        return (
            <div className="todos" >
                <h3>Todos</h3>
                {todoItems}
            </div>
        );
    }
}



export default Todos;
