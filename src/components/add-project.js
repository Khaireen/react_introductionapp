import React, { Component } from 'react';
import uuid from 'uuid'


class AddProject extends Component {
    constructor() {
        super();
        this.state = {
            newProject: {}
        };
    }

    static defaultProps = {
        categories: ['Web design', 'Web development', 'Mobile dev']
    }

    handleSubmit(e) {
        if (!this.refs.title.value) {
            alert('Please add title')
        } else {
            this.setState({
                newProject: {
                    id: uuid.v4(),
                    title: this.refs.title.value,
                    category: this.refs.category.value
                }
            }, function () {
                this.props.addProject(this.state.newProject);
            });
        }
        e.preventDefault();
    }

    render() {
        let categoryOptions = this.props.categories.map(category => {
            return <option key={category} value={category}>{category}</option>
        });
        return (
            <div >
                <h3>Add project</h3>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div>
                        <label>Title</label><br />
                        <input type="text" ref="title" />
                    </div>
                    <br />
                    <div>
                        <label>Category</label><br />
                        <select type="text" ref="category">
                            {categoryOptions}
                        </select>
                    </div>
                    <br />
                    <div>
                        <input type="submit" value="Submit" />
                    </div>
                </form>
                <br />
            </div>
        );
    }
}


export default AddProject;
