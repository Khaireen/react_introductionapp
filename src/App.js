import React, { Component } from 'react';
import uuid from 'uuid';
import Projects from './components/projects';
import AddProject from './components/add-project';
import Todos from './components/todos';


class App extends Component {
    constructor() {
        super();
        this.state = {
            projects: [],
            todos: []
        }
    }

    getTodos() {
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(response => response.json())
            .then(todos => this.setState({ todos }))
            .catch(error => console.log(error))
    }

    getProjects() {
        this.setState({
            projects: [
                {
                    id: uuid.v4(),
                    title: 'Business website',
                    category: 'Web design'
                },
                {
                    id: uuid.v4(),
                    title: 'Social app',
                    category: 'Mobile dev'
                },
                {
                    id: uuid.v4(),
                    title: 'E-commerce shopping cart',
                    category: 'Web development'
                }
            ]
        });
    }

    componentDidMount() {
        this.getTodos();
        this.getProjects();
    }

    handleAddProject(project) {
        let projects = this.state.projects;
        projects.push(project);
        this.setState({ projects: projects });
    }

    handleDeleteProject(id) {
        let projects = this.state.projects;
        let index = projects.findIndex(x => x.id === id);
        projects.splice(index, 1);
        this.setState({ projects: projects });
    }

    render() {
        return (
            <div className="App">
                <AddProject addProject={this.handleAddProject.bind(this)} />
                <Projects projects={this.state.projects} onDelete={this.handleDeleteProject.bind(this)} />
                <hr />
                <Todos todos={this.state.todos} />
            </div>
        );
    }
}

export default App;
